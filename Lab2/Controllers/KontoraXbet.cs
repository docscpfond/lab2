﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.Controllers
{
    [ApiController]
    [Route("bids")]
    public class KontoraXbet : ControllerBase
    {
       private readonly ILogger<KontoraXbet> _logger;

        public KontoraXbet(ILogger<KontoraXbet> logger)
        {
            _logger = logger;
        }

        static List<InfaBid> bids = new List<InfaBid>() {
        new InfaBid (1, "Ukraine vs France", 5, 500),
        new InfaBid (2, "France vs Germani", 2, 1900),
        new InfaBid (3, "Ukraine vs Germani", 4, 9000)
            };

        [HttpGet]
        public IEnumerable<InfaBid> Get()
        {
            return bids;
        }

        [HttpGet("{id}")]
        public ActionResult<InfaBid> Get(int id)
        {
            var newbids = bids.FirstOrDefault((p) => p.Id == id);
            if (newbids == null)
            {
                return NotFound();
            }
            return newbids;
        }

        [HttpPost]
        public IEnumerable<InfaBid> Post([FromBody] InfaBid bid)
        {
            bid.Id = bids.Max(x => x.Id) + 1;
            bids.Add(bid);
            return bids;
        }

        [HttpDelete("{id}")]
        public IEnumerable<InfaBid> Delete(int id)
        {
            var newbids = bids.FindIndex(i => i.Id == id);
            if (newbids < 0)
            {
                return bids;
            }
            bids.RemoveAt (newbids);
            return bids;
        }

        [HttpPut("{id}")]
        public IEnumerable<InfaBid> Put(int id, [FromBody] float value)
        {
            var newbids = bids.FindIndex(i => i.Id == id);
            if (newbids < 0)
            {
                return bids;
            }
            var bid = bids[newbids];
            bid.Price = value;
            return bids;
        }


    }
}
